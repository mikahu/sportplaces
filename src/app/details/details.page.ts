import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { PlaceService } from '../place.service';
import { Place } from '../../place';

@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})

export class DetailsPage implements OnInit {

  title: string;
  text: string;
  id: number;
  pSportPlace: any; 
  placeDetails = Array<Place>();
  
  constructor(    public activatedRoute: ActivatedRoute,
                  private router: Router,
                  public placeService: PlaceService     ) { }
                
  ngOnInit() {
    this.placeDetails = this.placeService.placeDetails;
  }

  show_information() {
    this.router.navigateByUrl('information');
  }

  show_map() {
    this.router.navigateByUrl('map');
  }

  openUrl( url: string ){
    // window.open('https://www.openstreetmap.org/copyright', '_system');
    window.open(url, '_system');
  }

}

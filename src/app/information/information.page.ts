import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-information',
  templateUrl: './information.page.html',
  styleUrls: ['./information.page.scss'],
})
export class InformationPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  openUrl(){
    window.open('https://www.openstreetmap.org/copyright', '_system');
  }

}

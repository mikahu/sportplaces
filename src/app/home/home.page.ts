import { Component, OnInit, ɵConsole } from '@angular/core';
import { ConnectionService } from '../connection.service';
import { Router } from '@angular/router';
import { PlaceService } from '../place.service';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{

  sportPlaces: any;
  userInput: string;
  mainHeadText: string;
  nameHeadText: string;
  typeHeadText: string;
  cityCode: number;
  private local_URL : string = 'assets/citycodes.json';

  constructor(    public connectionService: ConnectionService,
                  private router: Router,
                  public placeService: PlaceService,
                  public toastController: ToastController     ){}

  ngOnInit() {
    this.mainHeadText = "";
    this.nameHeadText = "";
    this.typeHeadText = "";
    this.sportPlaces = null;
  }

  onClickDetails( sportPlace: any ){
    this.placeService.fillDetails( sportPlace );
    this.router.navigateByUrl('details');
  }

  find(e) {

    this.mainHeadText = "";
    this.nameHeadText = "";
    this.typeHeadText = "";
    this.sportPlaces = null;

    if( e.keyCode === 13 ) {

      this.connectionService.getJSON( this.local_URL ).subscribe( localJson => {

        localJson.forEach( element => {

          if( this.checkStringsSimilarity( this.userInput, element.cityName ) === 0 ) {
            this.mainHeadText = this.userInput;
            this.nameHeadText = "Liikuntapaikka";
            this.typeHeadText = "Tyyppi";
            this.cityCode = element.cityCode;
            this.placeService.fetchJsonFromServer( this.cityCode );

            setTimeout( () => {
              this.sportPlaces = this.placeService.getPlaces().sort(this.sortJsonArray("name"));
            }, 2000);

          }

          // else {
          //   this.showToast( this.userInput );
          // }

        });
      });

    }
  }

  private checkStringsSimilarity( input: string, city: string ) {
    return input.localeCompare(city, undefined, { sensitivity: 'base' });
  }
  
  private sortJsonArray( property: any ) {    
    
    return function(a, b) {    
      if (a[property] > b[property]) {    
          return 1;    
      } else if (a[property] < b[property]) {    
          return -1;    
      } 
      return 0;    
    }    
  }

  async showToast( userInput: string ) {

    let tempStr = userInput.toUpperCase();

    const toast = await this.toastController.create({
      header: `Antamallesi kunnalle ${userInput} löydy kuntakoodia. 
              Tarkista kirjoitusasu!`,
      duration: 5000,
      position: 'top',
      buttons: [{
        text: 'Close',
        role: 'cancel'
      }]
    });

    await toast.present();
  }

  show_information() {
    this.router.navigateByUrl('information');
  }

}

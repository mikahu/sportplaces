import { TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { LocationService } from './location.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';


describe('LocationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule
      ],
      providers : [{
        provide: Geolocation
      }],
    });
  });

  it('should be created', () => {
    const service: LocationService = TestBed.get(LocationService);
    expect(service).toBeTruthy();
  });
});
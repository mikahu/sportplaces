import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocationService } from '../location.service';
import { PlaceService } from '../place.service';
import { ToastController } from '@ionic/angular';
import { OpenStreetMapProvider } from 'leaflet-geosearch';
import { Map } from 'leaflet';
import L from 'leaflet';
import 'leaflet/dist/images/marker-shadow.png';
import 'leaflet/dist/images/marker-icon-2x.png';

@Component({
  selector: 'app-map',
  templateUrl: './map.page.html',
  styleUrls: ['./map.page.scss'],
})
export class MapPage implements OnInit {

  map: Map;
  latitude: number;
  longitude: number;
  timestamp: any;
  sportPlaceName: string;
  distanceText: string = "";

  constructor(  private locationService: LocationService,
                public placeService: PlaceService,
                private router: Router,
                public toastController: ToastController  ) { }

  ngOnInit() {

    setTimeout( () => {
      this.sportPlaceName = this.placeService.getName();
      this.latitude = this.locationService.getLatitude();
      this.longitude = this.locationService.getLongitude();
      this.timestamp = this.locationService.getTimestamp();

      this.loadMap();
    }, 50);

  }

  loadMap() {

    setTimeout( () => {

      this.map = L.map('map', {
        center: [	this.latitude, this.longitude ],
        zoom: 14
      });

      L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png?{foo}',
      {  
        foo: 'bar', attribution:
        'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>'
      }
      ).addTo(this.map);

      this.map.locate({
      }).on('locationfound', (e) => {
        
        const markerGroup = L.featureGroup();

        const marker: any = L.marker([ e.latitude, e.longitude ]).on( 'click', () => {
          alert('Oma sijaintisi kartalla.\nTiedot päivitetty tänään klo '
            + this.timestamp.toString().split(" ", 7)[4] + '.');
        });

        markerGroup.addLayer( marker );
        this.map.addLayer( markerGroup );

      });

      this.addSportplaceAsMarkers();
        
    }, 50);

  }

  private addSportplaceAsMarkers() {

    const markerGroup = L.featureGroup();
    
    if( this.placeService.getLatitude() !== -1 ) {
      this.addMarker( markerGroup, this.placeService.getLatitude(), this.placeService.getLongitude() );
      this.map.addLayer( markerGroup );

      this.calculateDistance( this.placeService.getLatitude(), this.placeService.getLongitude() );
    }
    else {
      this.convertAddressToCoordinates();

      setTimeout( () => {

        if( this.placeService.getLatitude() !== -1 ) {
          this.addMarker( markerGroup, this.placeService.getLatitude(), this.placeService.getLongitude() );
          this.map.addLayer( markerGroup );

          this.calculateDistance( this.placeService.getLatitude(), this.placeService.getLongitude() );
        }
        else {
          this.showToast( this.placeService.getName() );
        }
        
      }, 100);

    }
  }

  async showToast( sportPlaceName: string ) {

    let tempStr = sportPlaceName.toUpperCase();

    const toast = await this.toastController.create({
      header: `Sijaintia ei voi näyttää, koska kohteelle ${tempStr} ei löydy
        koordinaatteja eikä osoitetta!`,
      duration: 5000,
      // position: 'top',
      buttons: [{
        text: 'Close',
        role: 'cancel'
      }]
    });

    await toast.present();
  }

  private convertAddressToCoordinates() {

    const provider = new OpenStreetMapProvider();
    let queryAddress = this.placeService.getSportPlaceAddress();
    let queryPromise = provider.search({ query: queryAddress });

    // Wait promise answer
    queryPromise.then( value => {
      let i : number;

      for( i=0; i < value.length; i++ ){
        this.placeService.setCoordinates( value[i].y, value[i].x );
      };

    }, reason => {
      console.log( reason ); // Error case!
    });

  }

  private addMarker( markerGroup: any, lat: number, lng: number ) {

    var redIcon = new L.Icon({
      iconUrl: 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-red.png',
      shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
      iconSize: [25, 41],
      iconAnchor: [12, 41],
      popupAnchor: [1, -34],
      shadowSize: [41, 41]
    });
    
    const marker: any = L.marker([ lat, lng ], {icon: redIcon}).on( 'click', () => {
      alert( this.sportPlaceName + '\n' + this.placeService.getSportPlaceAddress() );
    });

    markerGroup.addLayer(marker);

    this.map.setView([ lat, lng ], 9 );

  }

  calculateDistance( lat: number, lng: number ) {

    let deviceLatLng = L.latLng( this.latitude, this.longitude );
    let sportPlaceLatLng = L.latLng( lat, lng );
    let distanceOfPlaces = 
      (deviceLatLng.distanceTo( sportPlaceLatLng ).toFixed(0)/1000).toString().split(".", 2);

    if( distanceOfPlaces.length === 2) {
      this.distanceText = "Etäisyys sijainnistasi kohteeseen on n. " + distanceOfPlaces[0] + ' km.';
    }
    else {
      this.distanceText = "Etäisyys sijainnistasi kohteeseen on alle kilometri.";
    }

  }

  show_information() {
    this.router.navigateByUrl('information');
  }

}
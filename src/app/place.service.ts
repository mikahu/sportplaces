import { Injectable, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ConnectionService } from './connection.service';
import { Place } from '../place';

@Injectable({
  providedIn: 'root'
})
export class PlaceService implements OnInit {

  private remote_URL: string;
  private url_text: string = 
    'https://cors-anywhere.herokuapp.com/' +
    'http://lipas.cc.jyu.fi/api/sports-places?fields=properties&fields=schoolUse&fields=email&fields=type.name&fields=location.sportsPlaces&fields=renovationYears&fields=admin&fields=location.coordinates.tm35fin&fields=www&fields=location.geometries&fields=name&fields=type.typeCode&fields=location.locationId&fields=constructionYear&fields=freeUse&fields=location.city.name&fields=lastModified&fields=location.postalCode&fields=location.postalOffice&fields=location.city.cityCode&fields=phoneNumber&fields=location.neighborhood&fields=owner&fields=location.coordinates.wgs84&fields=location.address&cityCodes=';
  
  sportPlaces: any; 
  placeDetails = Array<Place>();
  currentPlace = new Place();
  latitude: number;
  longitude: number;
  sportPlaceAddress: string;
  sportPlaceName: string;

  constructor(  public connectionService: ConnectionService ) { }

  ngOnInit() { }


  public fetchJsonFromServer( cityCode: number ){

    this.remote_URL = this.url_text + cityCode;

    this.connectionService.getJSON( this.remote_URL ).subscribe( jsonContent => {
      this.setPlaces( jsonContent );
    });

  }

  fillDetails( data: any ) {

    // empty array
    this.placeDetails.length = 0;

    let addressExists : Boolean = false;
    let cityCodeExists : Boolean = false;
    let cityNameExists : Boolean = false;
    let latitudeExists : Boolean = false;
    let longitudeExists : Boolean = false;
    let locationIdExists : Boolean = false;
    let postalCodeExists : Boolean = false;
    let postalOfficeExists : Boolean = false;
    let areaM2Exists : Boolean = false;
    let fieldLengthMExists : Boolean = false;
    let fieldWidthMExists : Boolean = false;
    let fieldsCountExists : Boolean = false;
    let lightingExists : Boolean = false;
    let surfaceMaterialExists : Boolean = false;
    let typeNameExists : Boolean = false;
    let typeCodeExists : Boolean = false;

    data.hasOwnProperty('admin') ? this.currentPlace.setAdmin( data.admin ) : this.currentPlace.setAdmin( "-" );
    data.hasOwnProperty('constructionYear') ? this.currentPlace.setConstructionYear( data.constructionYear ) : this.currentPlace.setConstructionYear( "-" );
    data.hasOwnProperty('email') ? this.currentPlace.setEmail( data.email ) : this.currentPlace.setEmail( "-" );
    data.hasOwnProperty('freeUse') ? this.currentPlace.setFreeUse( data.freeUse ) : this.currentPlace.setFreeUse( "-" );
    data.hasOwnProperty('lastModified') ? this.currentPlace.setLastModified( data.lastModified ) : this.currentPlace.setLastModified( "-" );
    data.hasOwnProperty('name') ? this.currentPlace.setSportPlaceName( data.name ) : this.currentPlace.setSportPlaceName( "-" );
    data.hasOwnProperty('owner') ? this.currentPlace.setOwner( data.owner ) : this.currentPlace.setOwner( "-" );
    data.hasOwnProperty('schoolUse') ? this.currentPlace.setSchoolUse( data.schoolUse ) : this.currentPlace.setSchoolUse( "-" );
    data.hasOwnProperty('sportsPlaceId') ? this.currentPlace.setSportsPlaceId( data.sportsPlaceId ) : this.currentPlace.setSportsPlaceId( "-" );
    data.hasOwnProperty('www') ? this.currentPlace.setInternetAddress( data.www ) : this.currentPlace.setInternetAddress( "-" );
    
    // checks properties of location-object
    if( data.hasOwnProperty('location') ) {
      console.log("LOCATION YES");

      if( data.location.hasOwnProperty( 'address' )){
        addressExists = true;
      }

      if( data.location.hasOwnProperty( 'city' )){ 
        
        if( data.location.city.hasOwnProperty( 'cityCode' )){
          cityCodeExists = true;
        }

        if( data.location.city.hasOwnProperty( 'name' )){
          cityNameExists = true;
        }
      }

      if( data.location.hasOwnProperty( 'coordinates' )){ 

        if( data.location.coordinates.hasOwnProperty( 'wgs84' )){ 
          
          if( data.location.coordinates.wgs84.hasOwnProperty( 'lat' )){ 
            latitudeExists = true;
          }

          if( data.location.coordinates.wgs84.hasOwnProperty( 'lon' )){ 
            longitudeExists = true;
          }
        }
      }

      if( data.location.hasOwnProperty( 'locationId' ) ){
        locationIdExists = true;
      }

      if( data.location.hasOwnProperty('postalCode') ){
        postalCodeExists = true;
      }

      if( data.location.hasOwnProperty('postalOffice') ){
        postalOfficeExists = true;
      }
    }

    // checks properties of properties-object
    if( data.hasOwnProperty('properties') ) {

      if( data.properties.hasOwnProperty( 'areaM2' ) ) {
        areaM2Exists = true;
      }

      if( data.properties.hasOwnProperty('fieldLengthM') ) {
        console.log("PROPERTY: " + data.properties.fieldLengthM );
        fieldLengthMExists = true;
      }

      if( data.properties.hasOwnProperty('fieldWidthM') ) {
        console.log("PROPERTY: " + data.properties.fieldWidthM )
        fieldWidthMExists = true;
      }

      if( data.properties.hasOwnProperty('fieldsCount') ) {
        fieldsCountExists = true;
      }

      if( data.properties.hasOwnProperty('ligthing') ) {
        lightingExists = true;
      }

      if( data.properties.hasOwnProperty('surfaceMaterial') ) {
        surfaceMaterialExists = true;
      }

    }

    if( data.hasOwnProperty('type') ) {

      if( data.type.hasOwnProperty( 'name' ) ) {
        typeNameExists = true;
      }

      if( data.type.hasOwnProperty( 'typeCode' ) ) {
        typeCodeExists = true;
      }

    }

    addressExists === true ? this.currentPlace.setAddress( data.location.address ) : this.currentPlace.setAddress( "-" );
    cityCodeExists === true ? this.currentPlace.setCityCode( data.location.city.cityCode ) : this.currentPlace.setCityCode( "-" );
    cityNameExists === true ? this.currentPlace.setCityName( data.location.city.name ) : this.currentPlace.setCityName( "-" );
    latitudeExists === true ? this.currentPlace.setLatitude(data.location.coordinates.wgs84.lat) : this.currentPlace.setLatitude(-1);
    longitudeExists === true ? this.currentPlace.setLongitude(data.location.coordinates.wgs84.lon) : this.currentPlace.setLongitude(-1);
    locationIdExists === true ? this.currentPlace.setLocationId( data.location.locationId ) : this.currentPlace.setLocationId( "-" );
    postalCodeExists === true ? this.currentPlace.setPostalCode( data.location.postalCode ) : this.currentPlace.setPostalCode( "-" );
    postalOfficeExists === true ? this.currentPlace.setPostalOffice( data.location.postalOffice ) : this.currentPlace.setPostalOffice( "" );

    areaM2Exists === true ? this.currentPlace.setAreaSquareMetre( data.properties.areaM2 ) : this.currentPlace.setAreaSquareMetre( "-" );
    fieldLengthMExists === true ? this.currentPlace.setFieldLength( data.properties.fieldLengthM ) : this.currentPlace.setFieldLength( "-" );
    fieldWidthMExists === true ? this.currentPlace.setFieldWidth( data.properties.fieldWidthM ) : this.currentPlace.setFieldWidth( "-" );
    fieldsCountExists === true ? this.currentPlace.setFieldsCount( data.properties.fieldsCount ) : this.currentPlace.setFieldsCount( "-" );
    lightingExists === true ? this.currentPlace.setLighting( data.properties.ligthing ) : this.currentPlace.setLighting( "-" );
    surfaceMaterialExists === true ? this.currentPlace.setSurfaceMaterial( data.properties.surfaceMaterial ) : this.currentPlace.setSurfaceMaterial( "-" );

    typeNameExists === true ? this.currentPlace.setTypeName( data.type.name ) : this.currentPlace.setTypeName( "-" );
    typeCodeExists === true ? this.currentPlace.setTypeCode( data.type.typeCode ) : this.currentPlace.setTypeCode( "-" );

    this.placeDetails.push( this.currentPlace );

    this.setCoordinates( this.currentPlace.getLatitude(), this.currentPlace.getLongitude() );
    this.setAddess( this.currentPlace.getAddress(), this.currentPlace.getPostalOffice() );
    this.setName( this.currentPlace.getSportPlaceName() );

  }
  
  public setCoordinates( latitude: number, longitude: number ){
    this.latitude = latitude;
    this.longitude = longitude;
  }

  private setAddess( address: string, postalOffice: string ){
    this.sportPlaceAddress = address + ", " + postalOffice;
  }

  private setName( name: string ){
    this.sportPlaceName = name;
  }

  public getLatitude(): number{
    return this.latitude;
  }

  public getLongitude(): number{
    return this.longitude;
  }

  public getSportPlaceAddress(): string{
    return this.sportPlaceAddress;
  }

  public getName(): string {
    return this.sportPlaceName;
  }

  private setPlaces( jsonData: any ){
    this.sportPlaces = jsonData;
  }

  public getPlaces(): any {
    return this.sportPlaces;
  }

}

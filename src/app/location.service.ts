import { Injectable } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  latitude: number;
  longitude: number;
  timestamp: Date;
  altitude: any;
  accuracy: any;
  altitudeAccuracy: any;
  heading: any;
  speed: any;

  constructor( private geolocation: Geolocation ) {

    this.geolocation.getCurrentPosition().then( data => {
      
      this.latitude = data.coords.latitude;
      this.longitude = data.coords.longitude;
      this.timestamp = new Date(data.timestamp);
      this.altitude =  data.coords.altitude;
      this.accuracy =  data.coords.accuracy;
      this.altitudeAccuracy = data.coords.altitudeAccuracy;
      this.heading = data.coords.heading;
      this.speed = data.coords.speed;
    });

  }

  public getLatitude() {
    return this.latitude;
  }

  public getLongitude() {
    return this.longitude;
  }

  public getTimestamp() {
    return this.timestamp;
  }

  public getAltitude() {
    return this.altitude;
  }
  
  public getAccuracy() {
    return this.accuracy;
  }

  public getAltitudeAccuracy() {
    return this.altitudeAccuracy;
  }

  public getHeading() {
    return this.heading;
  }

  public getSpeed() {
    return this.speed;
  }
  
}

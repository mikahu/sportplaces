export class Place {

    admin: string;
    constructionYear: string;
    email: string;
    freeUse: any;
    lastModified: string;
    address: string;
    cityCode: string;
    cityName: string;
    latitude: number;
    longitude: number;
    locationId: string;
    postalCode: string;
    postalOffice: string;
    sportPlaceName: string;
    owner: string;
    areaSquareMetre: string;
    fieldLength: string;
    fieldWidth: string;
    fieldsCount: string;
    lighting: any;
    surfaceMaterial: string;
    schoolUse: any;
    sportsPlaceId: string;
    typeName: string;
    typeCode: string;
    internetAddress: string;

    constructor() { }

    // SET FUNCTIONS
    public setAdmin( admin : string ) {
        console.log("ADMIN: " + admin );
        this.admin = admin;
    }

    public setConstructionYear( constructionYear: string ) {
        this.constructionYear = constructionYear;
    }

    public setEmail( email: string ) {
        this.email = email;
    }

    public setFreeUse( freeUse: any ) {
        
        if( freeUse === true ) {
            this.freeUse = "Kyllä";
        }
        else {
            this.freeUse = "Ei";
        }
    }

    public setLastModified( lastModified: string ) {
        
        if( lastModified !== "-") {
            var temp = lastModified.split(" ")[0].split("-");
            this.lastModified = temp[2] + "." + temp[1] + "." + temp[0];
        }
        else {
            this.lastModified = lastModified;
        }
    }

    public setAddress( location_address: string ) {
        this.address = location_address;
    }

    public setCityCode( location_city_cityCode: string ) {
        this.cityCode = location_city_cityCode;
    }

    public setCityName( location_city_name: string ) {
        this.cityName = location_city_name;
    }

    public setLatitude( location_coordinates_wgs84_lat: number ) {
        this.latitude = location_coordinates_wgs84_lat;
    }

    public setLongitude( location_coordinates_wgs84_lon: number ) {
        this.longitude = location_coordinates_wgs84_lon;
    }

    public setLocationId( location_locationId: string ) {
        this.locationId = location_locationId;
    }

    public setPostalCode( location_postalCode: string ) {
        this.postalCode = location_postalCode;
    }

    public setPostalOffice( location_postalOffice: string ) {
        this.postalOffice = location_postalOffice;
    }

    public setSportPlaceName( name: string ) {
        this.sportPlaceName = name;
    }

    public setOwner( owner: string ) {
        this.owner = owner;
    }

    public setAreaSquareMetre( data_properties_areaM2: string ) {
        this.areaSquareMetre = data_properties_areaM2;
    }

    public setFieldLength( data_properties_fieldLengthM: string ) {
        this.fieldLength = data_properties_fieldLengthM;
    }

    public setFieldWidth( data_properties_fieldWidthM: string ) {
        this.fieldWidth = data_properties_fieldWidthM;
    }

    public setFieldsCount( data_properties_fieldsCount: string ) {
        this.fieldsCount = data_properties_fieldsCount;
    }

    public setLighting( data_properties_ligthing: any ) {
                            
        if( data_properties_ligthing === true ) {
            this.lighting = "Kyllä";
        }
        else {
            this.lighting = "Ei";
        }
    }

    public setSurfaceMaterial( data_properties_surfaceMaterial: string) {
        this.surfaceMaterial = data_properties_surfaceMaterial;
    }

    public setSchoolUse( schoolUse: any ) {
        
        if( schoolUse === true ) {
            this.schoolUse = "Kyllä";
        }
        else {
            this.schoolUse = "Ei";
        }
    }

    public setSportsPlaceId( sportsPlaceId: string ) {
        this.sportsPlaceId = sportsPlaceId;
    }
        
    public setTypeName( type_name: string ) {
        this.typeName = type_name;
    }

    public setTypeCode( type_typeCode: string ) {
        this.typeCode = type_typeCode;
    }

    public setInternetAddress( www: string ) {
        this.internetAddress = www;
    }

    // GET FUNCTIONS
    public getLatitude(): number {
        return this.latitude;
    }

    public getLongitude(): number {
        return this.longitude;
    }
    public getAddress(): string {
        return this.address;
    }

    public getPostalCode(): string {
        return this.postalCode;
    }

    public getPostalOffice(): string {
        return this.postalOffice;
    }

    public getSportPlaceName(): string {
        return this.sportPlaceName;
    }

}